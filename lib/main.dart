import 'package:flutter/material.dart';
import 'package:pocket_mon_app/page/page_battle.dart';
import 'package:pocket_mon_app/page/page_battle_wait.dart';
import 'package:pocket_mon_app/page/page_index.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: PageBattleWait(),
    );
  }
}


