import 'package:dio/dio.dart';
import 'package:pocket_mon_app/config/config_pocketmon.dart';
import 'package:pocket_mon_app/model/battle/battle_stage_status_result.dart';
import 'package:pocket_mon_app/model/battle/commonResult.dart';

class RepoBattle{
 Future<BattleStageStatusResult> getCurrentState() async {
   Dio dio = Dio();

   String _baseUrl='$apiUrI/battle/battle/item';//엔드포인트

   final response = await dio.get(
       _baseUrl,
   options: Options(
     followRedirects: false,
     validateStatus: (status){
       print(status);
       return status == 200;
      })
   );
   print(response.data);
   return BattleStageStatusResult.fromJson(response.data);
 }

 Future<CommonResult> setStageIn(num id) async {
   Dio dio = Dio();
   String _baseUrl='$apiUrI/battle/battle-join/{id}';//엔드포인트

   final response = await dio.post(
     _baseUrl.replaceAll('{id}', id.toString()),
       options: Options(
           followRedirects: false,
           validateStatus: (status){
             return status == 200;
           })
   );
   return CommonResult.fromJson(response.data);
 }

 Future<CommonResult> delStageMonster(num stageId) async {
   Dio dio = Dio();

   String _baseUrl='$apiUrI/battle/battle/battle/del/{stageId}';//엔드포인트
   final response = await dio.delete(
       _baseUrl.replaceAll('{stageId}', stageId.toString()),
       options: Options(
           followRedirects: false,
           validateStatus: (status){
             return status == 200;
           })
   );
   return CommonResult.fromJson(response.data);
 }
}