import 'package:dio/dio.dart';
import 'package:pocket_mon_app/config/config_pocketmon.dart';
import 'package:pocket_mon_app/model/result_detail.dart';
import 'package:pocket_mon_app/model/result_list.dart';

class RepoPocketmon {
  Future<ResultList> getPocketmon() async {
    Dio dio = Dio();

    final String _baseUrl = baseUrI;

    final response = await dio.get(
      _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return ResultList.fromJson(response.data);
 }

 Future<ResultDetail> getPoketmonDetail(num id) async {
    Dio dio = Dio();

    final String _detailUrl='$apiUrI/pocket/detail/{id}';

    final response = await dio.get(
        _detailUrl.replaceAll('{id}', id.toString()),
    options: Options(
      followRedirects: false,
      validateStatus: (status) {
        print(7);
        return status == 200;
      }
    )
    );
    return ResultDetail.fromJson(response.data);
 }
}