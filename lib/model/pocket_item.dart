

class PocketItem{
    num id;
    String pocketImg;
    String pocketName;
    String pocketType;
    num offensive;
    num defensive;

    PocketItem(this.id, this.pocketImg, this.pocketName, this.pocketType, this.offensive, this.defensive);

    factory PocketItem.fromJson(Map < String , dynamic > json){
        return PocketItem(
        json['id'],
        json['pocketImg'],
        json['pocketName'],
        json['pocketType'],
        json['offensive'],
        json['defensive'],
        );
    }
}