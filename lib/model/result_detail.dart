

import 'package:pocket_mon_app/model/pocket_detail_item.dart';

class ResultDetail{
    String msg;
    num code;
    PocketDetailItem data;

    ResultDetail(this.msg,this.code,this.data);

    factory ResultDetail.fromJson(Map < String , dynamic > json){
      return ResultDetail(
          json['msg'],
          json['code'],
          PocketDetailItem.fromJson(json['data']));
    }
}