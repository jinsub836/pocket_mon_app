class BattleMonsterItem{
   num monsterBattleFieldId;
   num monsterId;
   String pocketImg;
   String monsterName;
   String monsterType;
   num monsterHp;
   num monsterSpeed;
   num monsterOffensive;
   num monsterDefensive;

   BattleMonsterItem(this.monsterBattleFieldId,this.monsterId,  this.pocketImg,  this.monsterName, this.monsterType,
       this.monsterHp, this.monsterSpeed, this.monsterOffensive, this.monsterDefensive);
   factory BattleMonsterItem.fromJson (Map<String,dynamic> json){
     return BattleMonsterItem(
         json['monsterBattleFieldId'],
         json['monsterId'],
         json['pocketImg'],
         json['monsterName'],
         json['monsterType'],
         json['monsterHp'],
         json['monsterSpeed'],
         json['monsterOffensive'],
         json['monsterDefensive']);
   }
}