import 'dart:convert';

import 'package:pocket_mon_app/model/battle/battle_monster_item.dart';

class BattleStageStatusResult{
  BattleMonsterItem? monster1;
  BattleMonsterItem? monster2;

  BattleStageStatusResult(this.monster1, this.monster2);

  factory BattleStageStatusResult.fromJson(Map<String,dynamic> json){
    return BattleStageStatusResult(
    json['monster1'] !=null ?
    BattleMonsterItem.fromJson(json['monster1']): null,
    json['monster2'] !=null ?
    BattleMonsterItem.fromJson(json['monster2']): null
    );
  }
}