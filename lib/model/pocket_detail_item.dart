class PocketDetailItem{
   String pocketImg;
   String pocketName;
   String pocketType;
   num hp;
   num speed;
   num offensive;
   num defensive;
   num evolutionGrade;
   String etcMemo;

   PocketDetailItem(this.pocketImg,this.pocketName,this.pocketType,this.hp,
       this.speed,this.offensive,this.defensive,this.evolutionGrade,this.etcMemo);

   factory PocketDetailItem.fromJson(Map< String, dynamic> json){
     return PocketDetailItem(
         json['pocketImg'],
         json['pocketName'],
         json['pocketType'],
         json['hp'],
         json['speed'],
         json['offensive'],
         json['defensive'],
         json['evolutionGrade'],
         json['etcMemo']);
   }
}