import 'package:pocket_mon_app/model/pocket_item.dart';

class ResultList{
  String msg;
  num code;
  List<PocketItem> list;
  num totalCount;

  ResultList(this.msg, this.code , this.list , this.totalCount);

  factory ResultList.fromJson(Map<String, dynamic> json){
    return ResultList(
      json['msg'],
      json['code'],
      json['list'] !=null ?
      (json['list'] as List).map((e) => PocketItem.fromJson(e)).toList() :
      [],
      json['totalCount']
    );
  }
}