
import 'package:flutter/material.dart';
import 'package:pocket_mon_app/component/component_battle_start.dart';
import 'package:pocket_mon_app/model/battle/battle_monster_item.dart';


class PageBattle extends StatefulWidget {
  const PageBattle({super.key,
  required this.monster1,
  required this.monster2});


  final BattleMonsterItem monster1;
  final BattleMonsterItem monster2;

  @override
  State<PageBattle> createState() => _PageBattleState();
}


class _PageBattleState extends State<PageBattle> {

  // 선빵 누구인지

  bool _isFirstTurn = true;
  bool _isAlive1 = true;
  num _monster1CurrentHp = 0;

  bool _isAlive2 = true;
  num _monster2CurrentHp = 0;

  num _Damage1 = 0;//초기 데미지
  num _Damage2 = 0;


  @override
  void initState() {
    super.initState();
    _calculateTurn();
    _monster1CurrentHp = widget.monster1.monsterHp;
    _monster2CurrentHp = widget.monster2.monsterHp;
  }

  void _calculateTurn(){
    if(widget.monster1.monsterSpeed < widget.monster2.monsterSpeed ){
      setState(() {
        _isFirstTurn = false;
      });
    }
  }



  num _calculateDamage(num myHitPower, num targetDefPower){
    List critical = [1,2,3,4,5,6,7,8,9];
    critical.shuffle();

    bool isCritical = false;
    if(critical[0]==1) isCritical = true;

    num resultHit = myHitPower ; // 공격력
    if (isCritical) resultHit = resultHit * 1.5; // 크리티컬이면 공격력 두배 뻥튀기
    resultHit = resultHit - targetDefPower* 0.9; // 상대방 방어력만큼 데미지 까주기
    resultHit = resultHit.round(); // 반올림 처리

    if(resultHit <= 0) resultHit = 1;

    return resultHit;}

// 몬스터 생사 여부 확인
  void _checkIsDead(num targetMonster){
    if(targetMonster ==1 && (_monster1CurrentHp <= 0)){
      setState(() {
        _isAlive1 = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp <= 0)){
      setState(() {
        _isAlive2 = false;
      });
    }
  }

  // 공격 처리
  void _attMonster (num actionMonster){
    num myHitPower = widget.monster1.monsterOffensive;
    num targetDefPower = widget.monster2.monsterDefensive;

    if (actionMonster == 2){
      myHitPower = widget.monster2.monsterOffensive;
      targetDefPower = widget.monster1.monsterOffensive;
    }

    num resultHit = _calculateDamage(myHitPower, targetDefPower);

    setState(() {
      if(actionMonster == 2){
        _Damage2 = resultHit;
      }else{
      _Damage1 = resultHit;}
    });

    if (actionMonster ==1){
      setState(() {
        _monster2CurrentHp -= resultHit;
        if(_monster2CurrentHp <= 0)_monster2CurrentHp = 0;
        _checkIsDead(2);
      });
    }else{
      setState(() {
        _monster1CurrentHp -= resultHit;
        if(_monster1CurrentHp <= 0)_monster1CurrentHp = 0;
        _checkIsDead(1);
      });
    }
    setState(() {
      _isFirstTurn = !_isFirstTurn;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar:AppBar(
          backgroundColor: Colors.red,
          title:Text('포켓몬스터'),
        ),
        body: _buildBody(context) );
  }

  Widget _buildBody(BuildContext context){

  double phoneWidth = MediaQuery.of(context).size.width;
  double phoneHeight = MediaQuery.of(context).size.height;
  return SingleChildScrollView(
    child: Container( width: phoneWidth,
      height: phoneHeight/1.2,
      decoration:
      BoxDecoration(
        image: DecorationImage(
        image :AssetImage('assets/battle.jpg'),
        fit: BoxFit.fill)
      ),
      child: Column(
        children: [
          Container(//1번째 포켓몬
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(top: 100, right: 80),
            child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Container(),
              Container(margin: EdgeInsets.only(left: 270),
                  child: Text('-$_Damage2',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.red
                  ),)),
                ComponentBattleStart(
                    monsterItem: widget.monster1,
                    callback: (){
                      _attMonster(1);
                    },
                    isMyTurn:_isFirstTurn,
                    isAlive:_isAlive1,
                    currentHp: _monster1CurrentHp,),
              ],
            )
          ),
          Container( margin: EdgeInsets.only(left: 70,bottom:100 ),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child:
                    ComponentBattleStart(
                        monsterItem: widget.monster2,
                        callback: (){
                          _attMonster(2);
                        },
                        isMyTurn: !_isFirstTurn,
                        isAlive: _isAlive2,
                      currentHp:_monster2CurrentHp,)
                ),
                Container( margin: EdgeInsets.only(right: 280),
                  child:
                  Text('-$_Damage1',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.red
                  ),),
                ),
                Container()
              ],
            ),
          )
        ]
      ),
    ),
  );
 }
}


