import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:pocket_mon_app/component/component_pocketmon.dart';
import 'package:pocket_mon_app/model/pocket_item.dart';
import 'package:pocket_mon_app/page/page_detail_pocketmon.dart';
import 'package:pocket_mon_app/repository/repo_pocketmon.dart';


class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {

  String searchValue = '';
  final List<String> _suggestions = [
  ];

  List<PocketItem> _list =
  [];

  Future<void> _loadList() async{
    await RepoPocketmon().getPocketmon()
        .then((res) => {
          setState(() {
      _list = res.list;
    })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState(){
    super.initState();
    _loadList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar:
    EasySearchBar(
        title: const Text('Example'
          , style: TextStyle
            (color: Colors.white,
              fontSize: 20,
              fontFamily: 'Lemon',
              fontWeight: FontWeight.w200),
        ),
        backgroundColor: Colors.brown,
        onSearch: (value) => setState(() => searchValue = value),
        actions: [
          IconButton(icon: const Icon(Icons.person), onPressed: () {})
        ],
        iconTheme: IconThemeData(color: Colors.white),
        suggestions: _suggestions
    ), drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                  height: 100,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.brown,
                    ),
                    child: Text('메뉴',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontFamily: 'Lemon'
                      ),),
                  )),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
        body: _buildBody(context));
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          children: [
      Container(
      child:Row( mainAxisAlignment: MainAxisAlignment.center,
      children:[
      Image.asset('main_logo.png'),
      Container( margin: EdgeInsets.only(left: 100),
          child: Image.asset('dogam.png'))])
    ),
    Container( width: 900, margin: EdgeInsets.only(top: 30),
    child: GridView.builder(
    physics: ScrollPhysics(),
    scrollDirection: Axis.vertical,
    shrinkWrap: true,
    itemCount: _list.length, //item 개수
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 3, //1 개의 행에 보여줄 item 개수
    childAspectRatio: 2 / 3, //item 의 가로 1, 세로 2 의 비율
    mainAxisSpacing: 10, //수평 Padding
    crossAxisSpacing: 10, //수직 Padding
    ),
    itemBuilder: (BuildContext context, int index) {
    //item 의 반목문 항목 형성
    return ComponentPocketmon(
    pocketItem: _list[index],
    callback: (){
    Navigator.of(context).push(MaterialPageRoute(builder: (context) =>
    PageDetailPocketMon(id:_list[index].id)));
    });
    },),
    )])
    );
  }
}


