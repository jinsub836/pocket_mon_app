import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:pocket_mon_app/component/component_battle.dart';
import 'package:pocket_mon_app/model/battle/battle_monster_item.dart';
import 'package:pocket_mon_app/page/page_battle.dart';
import 'package:pocket_mon_app/repository/repo_battle.dart';

class PageBattleWait extends StatefulWidget {
  const PageBattleWait({super.key});

  @override
  State<PageBattleWait> createState() => _PageBattleWaitState();
}

BattleMonsterItem? monsterItem1;
BattleMonsterItem? monsterItem2;


String searchValue = '';
final List<String> _suggestions = [];

class _PageBattleWaitState extends State<PageBattleWait> {
  BattleMonsterItem? monsterItem1;
  BattleMonsterItem? monsterItem2;



  Future<void> _loadBattleMonsterItem() async {
    await RepoBattle().getCurrentState()
        .then((res) => {
      setState((){
        monsterItem1= res.monster1;
        monsterItem2 = res.monster2;
        print(monsterItem1);
        print(monsterItem2);
      })
    }) .catchError((err) => {
      debugPrint(err), print(7)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadBattleMonsterItem();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold( backgroundColor: Colors.white,
        appBar:
        EasySearchBar(
            title: const Text('Example'
              ,style:TextStyle
                (color: Colors.white,fontSize: 20,fontFamily: 'Lemon', fontWeight:FontWeight.w200),
            ),
            backgroundColor: Colors.brown,
            onSearch: (value) => setState(() => searchValue = value),
            actions: [
              IconButton(icon: const Icon(Icons.person), onPressed: () {})
            ],
            iconTheme: IconThemeData(color: Colors.white),
            suggestions: _suggestions
        ),drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                  height: 100,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.brown,
                    ),
                    child: Text('메뉴',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontFamily:'Lemon'
                      ),),
                  )),
              ListTile(
                  title: Text('회원 정보'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
        body: _buildBody(context) );
  }

Widget _buildBody(BuildContext context){
  double phoneWidth = MediaQuery.of(context).size.width;
  return SingleChildScrollView(
    child: Container(
      decoration:
      BoxDecoration(
          image: DecorationImage(
              image :AssetImage('assets/images.jpeg'),
              fit: BoxFit.fill)
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(//1번째 포켓몬
            width: phoneWidth/2.5,
            margin: EdgeInsets.only(top: 20, right: 10),
            child: Container(
              child:
                  monsterItem1 != null ?
                  ComponentBattle(monsterItem: monsterItem1!, callback: (){}) :
                      Container(height: MediaQuery.of(context).size.height/11)
            ),
          ),
          Container( margin: EdgeInsets.only(left: 50,top: 20),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(//2번째포켓몬
                  width: phoneWidth/2.5,
                  child:
                  Container(
                    child:
                     monsterItem2 != null  ?
                     ComponentBattle(monsterItem: monsterItem2!, callback: (){}) :
                     Container(height: MediaQuery.of(context).size.height/10)
                  ),
                ),
                Container(margin: EdgeInsets.only(right: 100),
                  child:
                  monsterItem1 != null && monsterItem2 != null ?
                  OutlineGradientButton(
                    onTap:(){     Navigator.of(context).push(MaterialPageRoute(builder: (context) =>
                        PageBattle(monster1: monsterItem1!, monster2: monsterItem2!)));},
                    child: SizedBox(
                      width: 100,
                      height: 100,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.play_arrow, color: Colors.grey, size: 24),
                          Text('Battle Start', style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                    backgroundColor: Colors.white,
                    gradient: LinearGradient(
                      colors: [Colors.blueAccent, Colors.blueGrey],
                      begin: Alignment(-1, -1),
                      end: Alignment(2, 2),
                    ),
                    strokeWidth: 4,
                    padding: EdgeInsets.zero,
                    radius: Radius.circular(50),
                  ) : Container(height: MediaQuery.of(context).size.height/5)
                )
              ],
            ),
          )
        ],
      )
      ,
    ),
  );
 }
}

