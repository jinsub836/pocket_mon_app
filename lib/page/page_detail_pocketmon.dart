import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:pocket_mon_app/model/pocket_detail_item.dart';
import 'package:pocket_mon_app/repository/repo_pocketmon.dart';

class PageDetailPocketMon extends StatefulWidget {
  const PageDetailPocketMon({super.key,
    required this.id
  });

  final num id;

  @override
  State<PageDetailPocketMon> createState() => _PageDetailPocketMonState();
}

class _PageDetailPocketMonState extends State<PageDetailPocketMon> {

  PocketDetailItem? _detail;

  Future<void> _loadDetail() async {
    await RepoPocketmon().getPoketmonDetail(widget.id)
        .then((value) =>
        setState(() {
          _detail = value.data;
        })).catchError((err) => {debugPrint(err)});
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }


  String searchValue = '';
  final List<String> _suggestions = [

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
        EasySearchBar(
            title: const Text('Example'
              ,style:TextStyle
                (color: Colors.white,fontSize: 20,fontFamily: 'Lemon', fontWeight:FontWeight.w200),
            ),
            backgroundColor: Colors.brown,
            onSearch: (value) => setState(() => searchValue = value),
            actions: [
              IconButton(icon: const Icon(Icons.person), onPressed: () {})
            ],
            iconTheme: IconThemeData(color: Colors.white),
            suggestions: _suggestions
        ),drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              Container(
                  height: 100,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: Colors.brown,
                    ),
                    child: Text('메뉴',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontFamily:'Lemon'
                      ),),
                  )),
              ListTile(
                  title: Text('회원 정보'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: Text(''),
                  onTap: () => Navigator.pop(context)
              )
            ]
        )
    ),
        body: _buildBody(context) );
  }


  Widget _buildBody(BuildContext context){
    if (_detail == null){return Text('로딩중');}
    else{
    return SingleChildScrollView(
        child: Container( margin: EdgeInsets.only(top: 100), width: 900,
          child: Column(mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container( width: 900,
                  child:
               Center(child:
              Image.asset('assets/${_detail!.pocketImg}',width: 400,fit: BoxFit.cover,),)),
              Row( mainAxisAlignment: MainAxisAlignment.center,
                children:[
              Container( width: 80,
                decoration:BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Colors.black
                    ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('No.')),
                    Container(child: Text('${widget.id}')
                    )],
                ),
              ),
              Container( width: 80,  decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('포켓몬이름')),
                    Container(child: Text('${_detail!.pocketName}'))
                  ],
                ),
              ),
              Container( width: 80,  decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('속성')),
                    Container(child: Text('${_detail!.pocketType}'))
                  ],
                ),
              ),
              Container( width: 80, decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('HP')),
                    Container(child: Text('${_detail!.hp}'))
                  ],
                ),
              ),
              Container( width: 80, decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('SPEED')),
                    Container(child: Text('${_detail!.speed}'))
                  ],
                ),
              ),
              Container( width: 80, decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('공격력')),
                    Container(child: Text('${_detail!.offensive}'))
                  ],
                ),
              ),
              Container( width: 80, decoration:BoxDecoration(
                  border: Border.all(
                      width: 1,
                      color: Colors.black
                  ) ) ,
                child: Column( mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container( width: 80,
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide( width: 1,
                                    color: Colors.black)
                            ) ) ,child: Text('방어력')),
                    Container(child: Text('${_detail!.defensive}'))
                  ],
                ),
              ),              Container( width: 80, decoration:BoxDecoration(
                      border: Border.all(
                          width: 1,
                          color: Colors.black
                      ) ) ,
                    child: Column( mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container( width: 80,
                            decoration:BoxDecoration(
                                border: Border(
                                    bottom: BorderSide( width: 1,
                                        color: Colors.black)
                                ) ) ,child: Text('진화단계')),
                        Container(child: Text('${_detail!.evolutionGrade}'))
                      ],
                    ),
                  )]
              )],
          ),
        ),
    );
  }}



}
