import 'package:flutter/material.dart';
import 'package:pocket_mon_app/model/pocket_item.dart';

class ComponentPocketmon extends StatelessWidget {
  const ComponentPocketmon({
    super.key,
  required this.pocketItem,
  required this.callback
  });

  final PocketItem pocketItem;
  final VoidCallback callback;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container( width: 200,
        child: Column(
          children: [
            Container(
              child: Image.asset('assets/${pocketItem.pocketImg}'),
            ),
            Container( width: 200,
              child: Row( mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(child: Text('No.')),
                  Container(child: Text('${pocketItem.id}'))
                ],
              ),
            ),
            Container(  width: 130,
              child: Row( mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(child: Text(pocketItem.pocketName))
                ],
              ),
            ),
            Container(  width: 100,
              child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(child: Text('속성')),
                  Container(child: Text(pocketItem.pocketType))
                ],
              ),
            ),
            Container(  width: 100,
              child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(child: Text('공격력')),
                  Container(child: Text('${pocketItem.offensive}'))
                ],
              ),
            ),
            Container(  width: 100,
              child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(child: Text('방어력')),
                  Container(child: Text('${pocketItem.defensive}'))
                ],
              ),
            )],
            )
        ),
    );
  }
}
