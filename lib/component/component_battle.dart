import 'package:flutter/material.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:pocket_mon_app/model/battle/battle_monster_item.dart';
import 'package:pocket_mon_app/page/page_battle_wait.dart';

class ComponentBattle extends StatefulWidget {
  const ComponentBattle({super.key,
  required this.monsterItem,
  required this.callback
  });

  final BattleMonsterItem monsterItem;
  final VoidCallback  callback;


  @override
  State<ComponentBattle> createState() => _ComponentBattleState();
}

class _ComponentBattleState extends State<ComponentBattle> {
  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;
    return
      Container(//1번째 포켓몬
      width: phoneWidth/2,
      margin: EdgeInsets.only(top: 10, right: 10),
      child: Column( mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(width: phoneWidth/2.5,
            child: Image.asset('assets/${widget.monsterItem.pocketImg}',width: phoneWidth/5
              ,height: phoneHeight/5,
            fit: BoxFit.fill,),
          ),
          Container(color: Colors.white,
            width: phoneHeight/5,
            child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  child: Text('이름:${widget.monsterItem.monsterName}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold
                  ),),
                ),
                Row(mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(right: 20),
                      child: Text('체  력:${widget.monsterItem.monsterHp}',
                      style: TextStyle(
                        fontSize: 12
                      ),),
                    ),
                    Container( alignment: Alignment.centerLeft,
                      child: Text('스피드:${widget.monsterItem.monsterSpeed}',
                      style: TextStyle(
                        fontSize: 12
                      ),),
                    ),
                  ],
                ),
                Row( mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container( alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(right: 20),
                      child: Text('공격력: ${widget.monsterItem.monsterOffensive}',
                      style: TextStyle(
                        fontSize: 12
                      ),),
                    ),
                    Container( alignment: Alignment.centerLeft,
                      child: Text('방어력:${widget.monsterItem.monsterDefensive}',
                      style: TextStyle(
                        fontSize: 12
                      ),),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container( margin: EdgeInsets.only(top: 20),
              child: OutlineGradientButton(
                child: Text('OUT'),
                gradient: LinearGradient(colors: [Colors.blueAccent, Colors.blueGrey]),
                strokeWidth: 4,
                backgroundColor: Colors.white,
                radius: Radius.circular(8),
              )
          ),],
      ),
    );
  }
}
