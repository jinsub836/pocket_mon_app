import 'package:flutter/material.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:pocket_mon_app/model/battle/battle_monster_item.dart';

class ComponentBattleStart extends StatefulWidget {
  const ComponentBattleStart({super.key,
  required this.monsterItem,
  required this.callback,
  required this.isMyTurn,
  required this.isAlive,
  required this.currentHp,

  });

  final BattleMonsterItem monsterItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isAlive;
  final num currentHp;

  @override
  State<ComponentBattleStart> createState() => _ComponentBattleStartState();
}

class _ComponentBattleStartState extends State<ComponentBattleStart> {
  double _calculateHpPercent() {
    return (widget.currentHp / widget.monsterItem!.monsterHp);
  }


  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    return Column( crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container( margin: EdgeInsets.only(top: 10),
            alignment: Alignment.center,
            width: phoneWidth/5,
            height: 30,
            child:
            (widget.isAlive && widget.isMyTurn) ?
            OutlinedButton(
              onPressed: widget.callback,
              child: Text('공격하기',
                style:TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold
                ),),
            )
                :Container(height: 30,)),
        Container( margin: EdgeInsets.only(top: 5),
          child: Row( mainAxisAlignment: MainAxisAlignment.end,
              children:[
                Container(
                  child:
                  Text("HP"),
                ),
                Container(child: new LinearPercentIndicator(
                         width: MediaQuery.of(context).size.width/4,
                         animation: false,
                         lineHeight: 20.0,
                         percent: _calculateHpPercent(),
                         center: Text("${(_calculateHpPercent()*100).round()}% , (${widget.currentHp}/${widget.monsterItem!.monsterHp})"),
                          progressColor: _calculateHpPercent() < 0.4 ? Colors.redAccent : Colors.greenAccent,
                        ),
                        ),
              ]),
        ),
        Container(
          alignment: Alignment.centerRight,
          width: phoneWidth/3.5,
          child: Image.asset('${widget.isAlive ? 'assets/${widget.monsterItem!.pocketImg}' : 'assets/monsterball.jpeg'}',width: phoneWidth/3.5,
              fit: BoxFit.fill),
        ),
      ],
    );
}}
